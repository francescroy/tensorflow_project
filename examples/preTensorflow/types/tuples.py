

# TUPLES

# Tuple items are ordered, unchangeable, and allow duplicate values.

thistuple = ("apple", "banana", "cherry")

#print(thistuple)

print(thistuple[1])

# Tuple to list (and viceversa is posible) 

thislist = list(thistuple)

print(type(thistuple))
