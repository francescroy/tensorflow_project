

# PYTHON LISTS

# List items are ordered, changeable, and allow duplicate values.

thislist = ["apple", "banana", "cherry", "orange", "kiwi", "melon", "mango"]

#print(thislist[2:5])
#print(thislist[0])
#print(thislist[2:]) 

thislist[0] = "banana"

if "apple" not in thislist:
    print("Yes, 'apple' is not in the fruits list") 

for x in thislist:
    print(x , end=" ") 
print("\n")

#for i in range(len(thislist)):
#    print(thislist[i]) 

list1 = ["a", "b", "c"]
list2 = [1, 2, 3]
list3 = list1 + list2
print(list3) 

print(type(thislist))

# es poden barrejar types? en una list?