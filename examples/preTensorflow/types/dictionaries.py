

# DICTIONARIES

# Dictionary items are ordered, changeable, and does not allow duplicates.

thisdict =	{
    "brand": "Ford",
    "model": "Mustang",
    "year": 1964
}
x = thisdict["model"]

print(x)

print(type(thisdict))