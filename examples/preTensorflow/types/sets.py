

# SETS

# Set items are unordered, unchangeable, and do not allow duplicate values.
# Note: Set items are unchangeable, but you can remove items and add new items.

thisset = {"apple", "banana", "cherry", "apple"}

print(thisset)

print(type(thisset))