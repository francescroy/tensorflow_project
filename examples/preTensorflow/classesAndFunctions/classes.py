

class MyClass:
    x = 5

p1 = MyClass()
print(p1.x) 


class Person:
    weight = 55
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def __str__(self):
        return f"{self.name}({self.age})" # per concatenar...
    def myfunc(self):
        print("Hello my name is " + self.name)

p1 = Person("John", 36)

print(p1) 
print(p1.name)
print(p1.age) 
p1.myfunc()
print(type(p1))