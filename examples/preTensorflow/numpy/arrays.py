
import numpy as np


arr = np.array([1, 2, 3, 4, 5]) # sino fas as np, doncs numpy.array()

print(type(arr)) 
print(arr.dtype) 

a = np.array(42)
b = np.array([1, 2, 3, 4, 5])
c = np.array([[1, 2, 3], [4, 5, 6]])
d = np.array([[[1, 2, 3], [4, 5, 6]], [[7, 8, 9], [10, 11, 12]]])

print(a.ndim)
print(b.ndim)
print(c.ndim)
print(d.ndim)       

#OOOOOOR:

print(d.shape)



print(d[0, 1, 2]) 
print(arr[3:]) 

arr2 = arr
arr2[0] = 100
print(arr[0])


for x in range(c.shape[0]):
    for y in range(c.shape[1]):
        print(c[x][y], end=' ')
    print()




 